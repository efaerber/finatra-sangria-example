lazy val root = (project in file(".")).
  settings(
    name := "finatra test",
    version := "0.0.1",
    scalaVersion := "2.11.7"
  )

  resolvers += "Twitter" at "http://maven.twttr.com"

  libraryDependencies ++= Seq(
    "com.twitter.finatra" %% "finatra-http" % "2.1.1",
    "com.twitter.finatra" %% "finatra-slf4j" % "2.1.1",
    "com.twitter" %% "bijection-util" % "0.8.1",
    "ch.qos.logback" % "logback-classic" % "1.1.2",
    "org.sangria-graphql" %% "sangria" % "0.4.3"
  )
