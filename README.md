Run with

    sbt run

You can test with

    curl -i 'http://localhost:8888/graphql' -H 'Content-Type: application/json' --data-binary '{"query" : "{ allHumans { name, friends { name } } }"}'
