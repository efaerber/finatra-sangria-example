package demo.controller

import scala.util.{Success, Failure}

import com.twitter.finatra.http.Controller
import sangria.parser.QueryParser
import sangria.execution.Executor
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.duration.Duration

import demo.request.GraphqlRequest
import demo.schema._
import demo.data._

class ExampleController extends Controller {
  val executor = Executor(
    schema = SchemaDefinition.StarWarsSchema,
    userContext = new CharacterRepo,
    deferredResolver = new FriendsResolver
  )

  post("/graphql") { graphqlRequest: GraphqlRequest =>
    QueryParser.parse(graphqlRequest.query) match {
      case Success(queryAst) => {
        val execution = executor.execute(
          queryAst = queryAst
        )
        val result = Await.result(execution, Duration.Inf)
        response.ok.json(result)
      }
      case Failure(error) => response.badRequest(error.getMessage())
    }
  }
}
