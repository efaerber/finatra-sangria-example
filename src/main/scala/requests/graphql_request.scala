package demo.request

case class GraphqlRequest(query: String, variables: Option[Map[String, Any]] = None, operation: Option[String] = None)
